from django.db.models import Q
from django.shortcuts import render, redirect,get_object_or_404, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate, login, logout
# from .forms import *
from django.contrib.auth.models import Group, User
from .models import *
from django.views.generic import CreateView
# from user.forms import UserProfileForm
from validate_email import validate_email #pip install validate_email

# Create your views here.
def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        # remember_me = request.POST.get('remember_me')
        if not request.POST.get('remember_me', None):
            request.session.set_expiry(0)
        # if remember_me:
        #         request.session.set_expiry(1209600) #2 weeks
        
        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            
            
            return HttpResponseRedirect('/')  #also can be redirect
        else:
            messages.info(request, 'Username or Password is incorrect,Please check again')

    context = {}

    return render(request, 'accounts/login.html')

def registerPage(request):
    if request.method == 'POST':
        context = {'has_error': False, 'data':request.POST}
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        password1 = request.POST.get('password1')

        if not request.POST.get('remember_me', None):
            request.session.set_expiry(0)
        
        if len(password)<6:
            messages.add_message(request, messages.ERROR, 'Password should be at least 6 characters')
            context['has_error'] = True
        
        if password != password1:
            messages.add_message(request, messages.ERROR, 'Passwords mismatch')
            context['has_error'] = True

        if not validate_email(email):
            messages.add_message(request, messages.ERROR, 'Enter a valid email')
            context['has_error'] = True

        if not username:
            messages.add_message(request, messages.ERROR, 'Username is required')
            context['has_error'] = True
        
        if User.objects.filter(username=username).exists():
            messages.add_message(request, messages.ERROR, 'Username is taken , chosen another one')
            context['has_error'] = True

        if User.objects.filter(email=email).exists():
            messages.add_message(request, messages.ERROR, 'Email is taken , chosen another one')
            context['has_error'] = True
        
        if context['has_error']:
            return render(request, 'accounts/login.html')

        user=User.objects.create_user(username=username, email=email)
        user.set_password(password)
        user.save()
        messages.add_message(request, messages.SUCCESS, 'Account was created')

    return render(request, 'accounts/register.html')

def logoutUser(request):
    logout(request)
    return redirect('account:login')