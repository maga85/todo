from django.db.models import Q
from django.shortcuts import render, redirect,get_object_or_404, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate, login, logout
from .forms import *
from django.contrib.auth.models import Group
from .models import *
from django.views.generic import CreateView
# from user.forms import UserProfileForm


def home(request):
    return render(request, 'mainpages/index.html')

@login_required(login_url='account:login')
def todos(request):
    todos = Todo.objects.filter(user=request.user).order_by('-id')
    todo_count = todos.count()
    context = {
        'todos':todos,
        'todo_count':todo_count,
    }
    return render(request, 'mainpages/todos.html', context)

@login_required(login_url='account:login')
def todoCreate(request):
    title='Create'
    form = TodoForm(request.POST or None, request.FILES or None)
    user = request.user
    if request.method == "POST":
        if form.is_valid():
            form.instance.user = user
            form.save()
            messages.success(request, "Your Todo has been created succesfully. Thank you")
            return redirect('todos')
    context = {
        'title':title,
        'form':form,
    }
    return render(request, 'mainpages/add-todo.html', context)

@login_required(login_url='account:login')
def todo_update(request, id):
    title='Update'
    todo = get_object_or_404(Todo, id=id)
    form = TodoForm(
        request.POST or None, 
        request.FILES or None, 
        instance=todo)
    user = request.user
    if request.method == "POST":
        if form.is_valid():
            todo = form.save(commit=False)
            form.instance.user = user
            form.save()
            messages.success(request, "Your Todo has been updated succesfully.")
            return redirect('todos')
    context = {
        'title': title,
        'form':form,
    }

    return render(request, 'mainpages/add-todo.html', context)

@login_required(login_url='account:login')
def todo_delete(request, id):
    todo = get_object_or_404(Todo, id=id)
    todo.delete()
    return redirect(reverse("todos"))

def error_404(request, exception):
    return render(request, 'errorpages/404.html', status=404)

def error_500(request):
    return render(request, 'errorpages/500.html', status=500)